---
layout: page
title: Meetings
permalink: /meetings/
---
To protect the health of our members and guests during the COVID-19 outbreak as well as comply with shelter-in-place orders we are meeting online every 1st and 3rd Tuesday at 7 p.m. If you are interested in participating please send us a note through our contact page.

If these days fall on a holiday and the group majority decides to forego the meeting, we will post a notice on the home page.
