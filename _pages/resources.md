---
layout: page
title: Resources
permalink: /resources/
comments: true
---

### APRS
+ [APRS.org](http://aprs.org){:target="_blank"}
+ [APRS.fi](https://aprs.fi){:target="_blank"}

### Navigation
+ [Cal Topo](http://caltopo.com){:target="_blank"}
+ [USGS Maps](https://nationalmap.gov/){:target="_blank"}
+ [USGS Map Viewer](https://viewer.nationalmap.gov/basic/){:target="_blank"}

### Frequencies and Repeaters  
+ [RadioReference](http://www.radioreference.com/){:target="_blank"}
+ [Repeaterbook](https://www.repeaterbook.com/index.php){:target="_blank"}

### WSPRnet  
+ [WSPRnet](http://wsprnet.org/drupal/){:target="_blank"}
+ [WSJT-X software download](http://physics.princeton.edu/pulsar/K1JT/wspr.html){:target="_blank"}

### GitLab
+ Downloadable file resources at the [DSARC GitLab](https://gitlab.com/dsarc){:target="_blank"}
+ [Chirp CSV's](https://gitlab.com/dsarc/chirp-csv){:target="_blank"} with local and national frequencies.

### Shared Documents
+ [Google Drive](https://drive.google.com/drive/folders/0B81z2DDvMhPwU21ORktnelBNYU0){:target="_blank"}

### Logging Software

#### Contesting
+ [N1MM](https://n1mm.hamdocs.com/tiki-index.php){:target="_blank"} Has alot of contests preinstalled and extras available to download. Though it is for Windows it does [run on Linux using Wine](https://www.scivision.co/n1mm-logger-linux-wine/){:target="_blank"}.
+ [FDLog](http://www.fdlog.info/){:target="_blank"} Preferred logging software for Field Day. It is written in Python 2 and runs on Windows, Linux, and Mac. Will also run on a Raspberry Pi.

#### General
+ [CHIRP](https://chirp.danplanet.com/projects/chirp/wiki/Home){:target="_blank"} CHIRP software for programming radios.

#### Radio and Communications History
+ [The History of the Car Radio](https://www.titlemax.com/articles/the-history-of-the-car-radio/){:target="_blank"} A great quick read on the progress of broadcast radio. (Submitted by Dylan)
