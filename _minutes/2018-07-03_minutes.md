---
layout: post
title: '2018-07-03 Meeting Minutes'
date: '2018-07-09 21:00:00'
tags:
- minutes
---

#### Do Something Amateur Radio Club  
#### Latah County Auxiliary Communications Team  
#### July 03, 2018  

**_Attendees_**    
*Nick Hewes, AF7ZJ  
*Bill Ward, K9GRZ  
*Austin Cole, KF7SIW  
*Heather Cole, KG7BUB  
*Glenn Boudreaux, KA5PTG  
*Bill Magnie, KI7TSV  
*Evan Hart, KG7WPM  
*Tom Storer, KI6DER  
*Micheal Sotolongo, KE7UDV  
*Parker Gibson, KI7UTI  
*Dave Schumacher, KF7CSV  
*Autumn St. Amand, KI7UTG  
*Randall Ramsey, KI7BSW  
*Sandee Schumacher, KA9UNA  
Ty Williams, W7TML  
Kelly Blackmon, NA5XX  
Bill Duncan, KE7OVE  
Christine Berven, KI7UTL  
Pat Blount, KE7SYF  
Demetre Nicholas, pending  
Marc Cramer, KG7DOG  
Mike Nealon, WA7MGN  
Skylar Johnson, Pending  
Bart Gableman, N1BAG  

* Present

**_Old Business_**  
Humane Society Run.  Five hams showed up to support the run.  Communications issues:  repeater had trouble so switched to simplex.  On HT’s the furthest station was iffy.  Otherwise, went well.  There was a mix of tactical vs. call signs.  We should be using tactical calls for this type of event.

Field Day.  Set up at Spring Valley with the NWRadio trailer and a couple of ancillary stations.  42 voice and 95 digital contacts.  Extra credit for power, visitors, GOTA, newspaper.  Total points claimed are 1064.  About 20 hams showed up plus about 8 guests.  Worked 22 states.  Improvements:  set up the ancillary stations closer together, work on antennas to minimize conflicts.  Evan thought setting up on the picnic table was a good way to draw people to the GOTA station.  More planning on where to set various parts of the station.  

Motion passed to reimburse Austin and Heather for expenses related to Field Day.  $191.89.  We got $78 in donations for food.

FONDO

July 21:  FondoPalouse.com 100 mile bike ride.  We will be supporting Fondo.  Sign up sheets have been sent to the PHARC club.  And is available here:

https://airtable.com/shrUW29aL8c5p8RBU

We’ll need about 30 participants, about 10 of them do not need to be licensed.


Training opportunities:

July is dedicated to Fondo.  August has the Jet Boat Races and Defcon so August is mostly shot, too.

Shoot for a mini-field day in September or October.   We need to line it up to take advantage of a national contest of some sort.

Tech Classes are scheduled thru Hell’s Gate ARC.  

August 18, 19:  tech class in 2B at the courthouse. 

Past Projects and Exercises:


Upcoming projects and exercises.  

We need to do some nighttime testing with HF.  We also need to find a way to establish a VHF/UHF link to Spokane and other surrounding areas.  

Event idea:  do a non-repeater exercise to determine who can talk to who on what equipment.

**_New Business_**  
* None this week.
