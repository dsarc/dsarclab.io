---
layout: post
title: '2018-07-17 Meeting Minutes'
date: '2018-07-17 19:00:00'
tags:
- minutes
---

#### Do Something Amateur Radio Club  
#### Latah County Auxiliary Communications Team  
#### July 17, 2018  

**_Attendees_**    
*Nick Hewes, AF7ZJ  
*Bill Ward, K9GRZ  
*Austin Cole, KF7SIW  
*Heather Cole, KG7BUB  
*Glenn Boudreaux, KA5PTG  
*Bill Magnie, KI7TSV  
*Evan Hart, KG7WPM  
*Dave Schumacher, KF7CSV  
*Randall Ramsey KI7BSW  
*Mike Nealon WA7MGN  
Tom Storer, KI6DER  
Kelly Blackmon, NA5XX  
Bill Duncan, KE7OVE  
Christine Berven, KI7UTL  
Micheal Sotolongo, KE7UDV  
Demetre Nicholas, pending  
Parker Gibson, KI7UTI  
Ty Williams, W7TML  
Pat Blount, KE7SYF  
Autumn St.Amand KI7UTG  
Bart Gableman N1BAG  
Marc Cramer KG7DOG  
Skylar Johnson Pending  
Sandee Schumacher, KA9UNA  

* Present

**_Treasurer’s Report_** 
* NA

**_Old Business_**  
Repeater.  AuxComm has been asked to write a proposal for a new repeater to be located on top of a local mountain.  The repeater should include or be capable of analog and DMR voice, APRS, packet messaging, links to other local repeaters.  Evan is leading the effort to write the proposal.  Sooner is better than later.

EVENTS

FONDO:  July 21:  FondoPalouse.com 100 mile bike ride.  We currently have 22 volunteers for Fondo!  We can always use a few more.  Sign up at: https://airtable.com/shrUW29aL8c5p8RBU

NCS will be in the AuxComm trailer at the fairgrounds.  Start/finish is at the Church of the Nazarene in Moscow at the corner of 6th and Mountain View.  

Most volunteers will need to be in position no later than 7AM (a few before that, a few later—see the assignment sheet) and should be done no later than 5PM.  In general, the radio operator should be one of the last people to leave any given location.  

Idaho Rally:  September 13 – 16 (I think the actual event is 14-16 but you can sign up for specific days, if needed), Placerville, Idaho.  This event takes about 50 hams for full coverage.   Besides doing a  Public Service, you just might get to see some really fast cars going really fast on forest roads!  This year it is sponsored by a national rally organization and there will be several factory teams—meaning more, faster, cars!  Sign up at http://www.rallydata.com/default.cfm

Because the rally is bigger this year, many of the festivities will take place in Idaho City which has more appropriate facilities.  Hams will (probably) still camp at or near Placerville which is also where NCS will be located.

Latah County Fair, Sept.  13 – 16.  We need to provide a presence.  Mike will work with Jim and see what we can get for facilities.

Training opportunities:

July is dedicated to Fondo.  August has the Jet Boat Races and Defcon so August is mostly shot, too.

Shoot for a mini-field day in September or October.   We need to line it up to take advantage of a national contest of some sort.

Several Tech Classes are scheduled thru Hell’s Gate ARC.  

We are hosting a tech class September 8 and 9  at the courthouse.   This date was moved from August.

Past Projects and Exercises:


Upcoming/potential projects and exercises.  

We need to do some nighttime testing with HF.  We also need to find a way to establish a VHF/UHF link to Spokane and other surrounding areas.  

Event idea:  do a non-repeater exercise to determine who can talk to who on what equipment.

AuxComm

Generators.  Two new Champion 3400 watt dual fuel generators and the parallel kit to connect them have been delivered and are in the trailer.  Bill and Austin checked them out and we can run pretty much everything in the trailer when the two generators are run in parallel.  The plan is to use them mostly on gas but we will have two propane tanks available.  There is one gas can which is to be stored EMPTY in the trailer with the cap loosened for venting.  At the end of an event, if there is still gas in the can, put it into the generators or put it in someone’s car so the can is empty for storage.

Vent hoods.  Two vent hoods have been installed so the ceiling vents can be left open.  This should significantly reduce the temperature in the trailer when in storage.

Tongue jack.  A power tongue jack is on order and should be installed by or during Fondo.

Portable Packet node.  An invoice for the parts to make a portable packet node has been submitted to Mike.  

Emergency dual band radio.  Mike has asked us to install an additional dual band radio in the trailer to be used for monitoring emergency frequencies.  This new radio will be programmed with the local LE freqs and installed such that it is not really easy to remove it.  The new radio will be coupled to a fold over antenna “permanently” mounted on the roof rails for very fast deployment.

Traffic control training.  Mike is asking MPD to provide traffic control training for AuxComm members.  Depending on how long the training is, it may be held at one of our meetings.  Additional trained traffic controllers are need for a variety of events around the county.  

**_New Business_**  
Hell's Gate ARC Tailgate Swap meet is Aug 18. No fee - (vendor space or entry). Church parking lot at 816 Sycamore st, bring a table, sell from a tailgate, or trunk. I would bring shade. The pastor of the church where we are holding swap meet is sick and will be selling water for donations for medical expenses. Set up at 0730 to start selling by 0900

Bill has been contacted by the Moscow Department of Parks and Recreation to see how we can support some of their larger activities.  Question for the group:  how aggressively do we want to pursue new event opportunities?  Events are likely our best recruiting tool for both new hams and reinvigorated experienced hams.  BUT:  as we start doing more events we may stress our current members until/unless we can actually recruit new help.
