---
layout: post
title: '2020-08-18 Meeting Minutes'
date: '2020-08-18 00:00:00'
tags:
- minutes
---

### Do Something Amateur Radio Club  
### Latah County Auxiliary Communications Team  
### August 18th, 2020  

#### **_Attendees_**    
**_August 4th_**  
* Austin KF7SIW  
* Mike N7ID  
* Michael KE7UDV
* Beka KJAPV  
* Dave KF7CSV  
* Nick AF7ZJ  

**_August 18th_**  
* Nick AF7ZJ  
* Austin KF7SIW  
* Bekah KJAPV  
* Bill KI7TSV  
* Dahmen KI7SIJ
* Mike N7ID  
* Dave KF7CSV  
* Sandee K9UNA  

* Present

#### **_Treasurer’s Report_**  
**_August 4th_** 
* SEL donation check of $250 deposited - we are up to $1018.96  - Changing delivery location to Mike's (WA7MGN) office as our current location is no longer available.



#### **_New Business_**  
**_August 4th_**  
* Skywarn updates.  
* DEFCON upcoming.  

**_August 18th_**  
* No topics. We had a round table for general radio chat and personal updates.
* The Latah County commercial building is full of stuff so we can't use it for in person license testing. Next month we will try to run testing outside at the fairgrounds.
